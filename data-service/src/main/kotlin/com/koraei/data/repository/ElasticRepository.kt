package com.koraei.data.repository

import com.koraei.data.document.ElasticClick
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository
import org.springframework.stereotype.Repository

@Repository
interface ElasticRepository : ElasticsearchRepository<ElasticClick, String> {
}
