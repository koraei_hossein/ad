package com.koraei.data.repository

import com.koraei.data.document.Impression
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ImpressionRepository : MongoRepository<Impression, String>  {
    fun findByRequestId(requestId : String) : Impression?
}