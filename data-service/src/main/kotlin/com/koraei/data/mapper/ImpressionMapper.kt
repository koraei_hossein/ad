package com.koraei.data.mapper

import com.koraei.data.document.Impression
import com.koraei.data.model.ImpressionModel

class ImpressionMapper {

    companion object {
        fun toModel(impression: Impression): ImpressionModel {

            return ImpressionModel(
                impression.requestId,
                impression.timestamp,
                impression.adId,
                impression.adTitle,
                impression.advertiserCost,
                impression.appId,
                impression.appTitle
            )
        }
    }

}