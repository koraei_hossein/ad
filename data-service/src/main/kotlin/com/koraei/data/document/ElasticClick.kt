package com.koraei.data.document

import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.Document

@Document(indexName = "elastic-index")
data class ElasticClick(
    @Id
    val requestId: String,
    val impressionTime: Long,
    val clickTime: Long,
    val adId: String,
    val adTitle: String,
    val advertiserCost: Double,
    val appId: String,
    val appTitle: String
) {
    override fun toString(): String {
        return "ElasticClick(requestId='$requestId', impressionTime=$impressionTime, clickTime=$clickTime, adId='$adId', adTitle='$adTitle', advertiserCost=$advertiserCost, appId='$appId', appTitle='$appTitle')"
    }
}