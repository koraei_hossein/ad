package com.koraei.data.model

class ClickModel(
    var requestId: String,
    var timestamp: Long
)
