package com.koraei.data.service

import com.koraei.data.model.ImpressionModel

interface ImpressionService {

    fun getByRequestId( requestId : String) : ImpressionModel?
}