package com.koraei.data.service

import com.koraei.data.model.ElasticClickModel

interface ElasticService {
    fun add(elasticClickModel: ElasticClickModel)
}