package com.koraei.data.service.impl

import com.koraei.data.document.ElasticClick
import com.koraei.data.model.ElasticClickModel
import com.koraei.data.repository.ElasticRepository
import com.koraei.data.service.ElasticService
import org.springframework.stereotype.Service

@Service
class ElasticClickServiceImpl(private val elasticRepository: ElasticRepository) : ElasticService {

    override fun add(elasticClickModel: ElasticClickModel) {
        val elasticClick = ElasticClick(
            requestId = elasticClickModel.requestId,
            impressionTime = elasticClickModel.impressionTime,
            adId = elasticClickModel.adId,
            adTitle = elasticClickModel.adTitle,
            advertiserCost = elasticClickModel.advertiserCost,
            appId = elasticClickModel.appId,
            appTitle = elasticClickModel.appTitle,
            clickTime = elasticClickModel.clickTime
        )

        elasticRepository.save(elasticClick)

    }
}