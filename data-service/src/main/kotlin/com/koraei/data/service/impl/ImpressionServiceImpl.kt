package com.koraei.data.service.impl

import com.google.gson.Gson
import com.koraei.data.document.Impression
import com.koraei.data.mapper.ImpressionMapper
import com.koraei.data.model.ImpressionModel
import com.koraei.data.repository.ImpressionRepository
import com.koraei.data.service.ImpressionService
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class ImpressionServiceImpl(private val impressionRepository: ImpressionRepository) :
    ImpressionService {

    @KafkaListener(topics = ["impression-event"], groupId = "group_id")
    fun getMessage(message: String) {

        val gson = Gson()
        val impressionModel = gson.fromJson(message, ImpressionModel::class.java)
        if (impressionModel != null) {
            impressionRepository.save(
                Impression(
                    requestId = impressionModel.requestId,
                    timestamp = impressionModel.timestamp,
                    adId = impressionModel.adId,
                    adTitle = impressionModel.adTitle,
                    advertiserCost = impressionModel.advertiserCost,
                    appId = impressionModel.appId,
                    appTitle = impressionModel.appTitle
                )
            )
        }
    }

    override fun getByRequestId(requestId: String): ImpressionModel? {
        val impression = impressionRepository.findByRequestId(requestId)
        if (impression != null)
            return ImpressionMapper.toModel(impression)
        else
            return null

    }
}