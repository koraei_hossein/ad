package com.koraei.data.service.impl

import com.google.gson.Gson
import com.koraei.data.exception.NotFoundException
import com.koraei.data.model.ClickModel
import com.koraei.data.model.ElasticClickModel
import com.koraei.data.service.ElasticService
import com.koraei.data.service.ImpressionService
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class ClickService(
    private val impressionService: ImpressionService,
    private val elasticService: ElasticService
) {

    @KafkaListener(topics = ["click-event"], groupId = "group_id")
    fun getMessage(message: String) {
        val gson = Gson()
        val clickModel = gson.fromJson(message, ClickModel::class.java)
        if (clickModel != null) {
            val impressionModel = impressionService.getByRequestId(clickModel.requestId)
            if (impressionModel != null) {
                val elasticClickModel = ElasticClickModel(
                    impressionModel.requestId,
                    impressionModel.timestamp,
                    clickModel.timestamp,
                    impressionModel.adId,
                    impressionModel.adTitle,
                    impressionModel.advertiserCost,
                    impressionModel.appId,
                    impressionModel.appTitle
                )
                elasticService.add(elasticClickModel)
                println("aaa")
            }
        }
    }
}