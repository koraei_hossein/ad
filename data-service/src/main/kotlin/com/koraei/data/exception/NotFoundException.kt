package com.koraei.data.exception

import org.springframework.http.HttpStatus

class NotFoundException : CustomException("not_found", HttpStatus.NOT_FOUND) {
}
