package com.koraei.data.exception

import org.springframework.http.HttpStatus

open class CustomException @JvmOverloads constructor(
    var msg: String = "error",
    var status: HttpStatus = HttpStatus.BAD_REQUEST,
    var data: Any? = null
) :
    RuntimeException() {

    override fun toString(): String {
        return String.format("%s => %s", msg, data)
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}