package com.koraei.data

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@SpringBootApplication
@EnableMongoRepositories("com.koraei.data")
class DataApplication

fun main(args: Array<String>) {
	runApplication<DataApplication>(*args)
}
