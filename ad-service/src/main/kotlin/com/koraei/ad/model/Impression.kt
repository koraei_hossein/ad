package com.koraei.ad.model

class Impression (
    var requestId: String,
    var timestamp: Long,
    var adId: String,
    var adTitle: String,
    var advertiserCost: Double,
    var appId: String,
    var appTitle: String,
)

