package com.koraei.ad.controller

import com.koraei.ad.domain.AddImpressionModel
import com.koraei.ad.service.ImpressionService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("impression-service/")
class ImpressionController (private val impressionService: ImpressionService){

    @PostMapping("/add")
    fun add(@RequestBody impressionModel:AddImpressionModel){
            impressionService.add(
                impressionModel.requestId,
                impressionModel.timestamp,
                impressionModel.adId,
                impressionModel.adTitle,
                impressionModel.advertiserCost,
                impressionModel.appId,
                impressionModel.appTitle
            )
    }
}