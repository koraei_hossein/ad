package com.koraei.ad.controller

import com.koraei.ad.domain.AddClickModel
import com.koraei.ad.service.ClickService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("click-service/")
class ClickController (private val clickService: ClickService){

    @PostMapping("/add")
    fun add(@RequestBody model: AddClickModel){
        clickService.add(
            model.requestId,
            model.timestamp
        )
    }
}