package com.koraei.ad.service

interface ImpressionService {
    fun add(requestId: String,
            timestamp: Long,
            adId: String,
            adTitle: String,
            advertiserCost: Double,
            appId: String,
            appTitle: String)
}