package com.koraei.ad.service.impl

import com.koraei.ad.model.Click
import com.koraei.ad.service.ClickService
import com.koraei.ad.service.KafkaService
import com.koraei.ad.service.SerializerService
import org.springframework.stereotype.Service

@Service
class ClickServiceImpl(
    private val kafkaService: KafkaService,
    private val serializerService: SerializerService
) : ClickService {

    val topic: String = "click-event"

    override fun add(requestId: String, timestamp: Long) {
        var click = Click(requestId, timestamp)

        kafkaService.sendMessage(serializerService.toJson(click), topic)
    }
}