package com.koraei.ad.service

interface ClickService {
    fun add(requestId: String,
            timestamp: Long)
}