package com.koraei.ad.service.impl

import com.koraei.ad.service.KafkaService
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.springframework.stereotype.Service
import java.util.concurrent.Future


@Service
class KafkaServiceImpl : KafkaService {

    override fun <T> sendMessage(message: T, topic: String) {
        var producerRecord :ProducerRecord<String, T> = ProducerRecord(topic, message)
        val map = mutableMapOf<String, String>()

        map["key.serializer"]   = "org.apache.kafka.common.serialization.StringSerializer"
        map["value.serializer"] = "org.apache.kafka.common.serialization.StringSerializer"
        map["bootstrap.servers"] = "localhost:9092"

        var producer = KafkaProducer<String, T>(map as Map<String, Any>?)

        var future:Future<RecordMetadata> = producer?.send(producerRecord)!!

    }


}