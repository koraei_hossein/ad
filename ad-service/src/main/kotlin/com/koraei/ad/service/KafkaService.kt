package com.koraei.ad.service

interface KafkaService {

    fun <T> sendMessage(message: T, topic: String)
}