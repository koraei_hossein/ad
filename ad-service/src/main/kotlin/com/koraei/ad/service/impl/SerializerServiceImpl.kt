package com.koraei.ad.service.impl

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.koraei.ad.service.SerializerService
import org.springframework.stereotype.Service
import java.io.IOException

@Service
class SerializerServiceImpl : SerializerService {

    private val mapper = ObjectMapper()

    override fun <T> toJson(obj: T): String? {
        return try {
            this.mapper.writeValueAsString(obj)
        } catch (e: JsonProcessingException) {
            null
        }
    }


    override fun <T> toObj(value: String?, classType: Class<T>?): T? {
        return try {
            this.mapper.readValue(value, classType)
        } catch (e: IOException) {
            null
        }
    }

}
