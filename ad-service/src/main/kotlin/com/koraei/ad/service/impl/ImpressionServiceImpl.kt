package com.koraei.ad.service.impl

import com.koraei.ad.model.Impression
import com.koraei.ad.service.ImpressionService
import com.koraei.ad.service.KafkaService
import com.koraei.ad.service.SerializerService
import org.springframework.stereotype.Component

@Component
class ImpressionServiceImpl(
    private val kafkaService: KafkaService,
    private val serializerService: SerializerService
) : ImpressionService {

    val topic: String = "impression-event"

    override fun add(
        requestId: String,
        timestamp: Long,
        adId: String,
        adTitle: String,
        advertiserCost: Double,
        appId: String,
        appTitle: String
    ) {
        var impression = Impression(
            requestId,
            timestamp,
            adId,
            adTitle,
            advertiserCost,
            appId,
            appTitle
        )
        kafkaService.sendMessage(serializerService.toJson(impression), topic)
    }

}