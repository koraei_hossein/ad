package com.koraei.ad.service

interface SerializerService {
    fun <T> toJson(obj: T): String?
    fun <T> toObj(value: String?, classType: Class<T>?): T?
}
