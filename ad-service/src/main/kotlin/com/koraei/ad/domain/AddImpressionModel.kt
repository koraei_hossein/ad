package com.koraei.ad.domain

class AddImpressionModel {
    val requestId: String = ""
    val timestamp: Long = 0
    val adId: String = ""
    val adTitle: String = ""
    val advertiserCost: Double = 0.0
    val appId: String = ""
    val appTitle: String = ""
}
