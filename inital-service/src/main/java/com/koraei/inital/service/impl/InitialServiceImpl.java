package com.koraei.inital.service.impl;

import com.koraei.inital.model.ClickModel;
import com.koraei.inital.model.ImpressionModel;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InitialServiceImpl {

  RestTemplate restTemplate = new RestTemplate();


  @Scheduled(fixedRate = 1000, initialDelay = 1000)
  public void initialImpression() {

    ImpressionModel impressionModel = new ImpressionModel(
        generateRandomString(true, true, 8),
        generateRandomLong(0, 1636224631),
        generateRandomString(true, true, 8),
        generateRandomString(true, true, 8),
        generateRandomDouble(1000, 50000),
        generateRandomString(true, true, 8),
        generateRandomString(true, true, 8));

    sendRequest(impressionModel);

    if (generateRandomBoolean()) {
        ClickModel clickModel = new ClickModel(
            impressionModel.getRequestId(),
            generateRandomLong(0, 1636224631)
        );
        sendRequest(clickModel);
    }

  }

  private String generateRandomString(boolean letter, boolean number, int length) {
    return RandomStringUtils.random(length, letter, number);
  }

  private long generateRandomLong(long min, long max) {
    return ThreadLocalRandom.current().nextLong(min, max);

  }

  private double generateRandomDouble(int min, int max) {
    return ThreadLocalRandom.current().nextDouble(min, max);
  }

  private Boolean generateRandomBoolean() {
    return ThreadLocalRandom.current().nextBoolean();
  }

  private HttpHeaders getHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Accept", "application/json");
    headers.add("Content-type", "application/json");
    return headers;
  }

  private <T> void sendRequest(T model) {
    HttpEntity<T> request =
        new HttpEntity<>(
            model
            , getHeaders());
    if (model instanceof ImpressionModel) {
      restTemplate
          .postForEntity("http://127.0.0.1:5010/impression-service/add",
              request,
              String.class);
    } else if (model instanceof ClickModel) {
      restTemplate
          .postForEntity("http://127.0.0.1:5010/click-service/add",
              request,
              String.class);
    }

  }

}
