package com.koraei.inital.model;

public class ImpressionModel {

  private String requestId;
  private Long timestamp;
  private String adId;
  private String adTitle;
  private Double advertiserCost;
  private String appId;
  private String appTitle;

  public ImpressionModel(String requestId,
      Long timestamp,
      String adId,
      String adTitle,
      Double advertiserCost,
      String appId,
      String appTitle) {
    this.requestId = requestId;
    this.timestamp = timestamp;
    this.adId = adId;
    this.adTitle = adTitle;
    this.advertiserCost = advertiserCost;
    this.appId = appId;
    this.appTitle = appTitle;
  }

  public String getRequestId() {
    return requestId;
  }

  public Long getTimestamp() {
    return timestamp;
  }

  public String getAdId() {
    return adId;
  }

  public String getAdTitle() {
    return adTitle;
  }

  public Double getAdvertiserCost() {
    return advertiserCost;
  }

  public String getAppId() {
    return appId;
  }

  public String getAppTitle() {
    return appTitle;
  }

  @Override
  public String toString() {
    return "ImpressionModel{" +
        "requestId='" + requestId + '\'' +
        ", timestamp=" + timestamp +
        ", adId='" + adId + '\'' +
        ", adTitle='" + adTitle + '\'' +
        ", advertiserCost=" + advertiserCost +
        ", appId='" + appId + '\'' +
        ", appTitle='" + appTitle + '\'' +
        '}';
  }
}
