package com.koraei.inital.model;

public class ClickModel {

  private String requestId;
  private Long timestamp;

  public ClickModel(String requestId, Long timestamp) {
    this.requestId = requestId;
    this.timestamp = timestamp;
  }

  public String getRequestId() {
    return requestId;
  }

  public Long getTimestamp() {
    return timestamp;
  }
}
